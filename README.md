# sterm

This is a [KISS](https://en.wikipedia.org/wiki/KISS_principle) serial terminal program.
It performs as little as possible translations.
