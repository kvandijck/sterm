PROGS	= sterm hexterm
default	: $(PROGS)

PREFIX	= /usr/local

CFLAGS	= -Wall
INSTOPT	= -s

-include config.mk

sterm: LDLIBS+=-lm

.PHONY: install clean

install: $(PROGS)
	$(foreach PROG, $(PROGS), install -vp -m 0777 $(INSTOPTS) $(PROG) $(DESTDIR)$(PREFIX)/bin/$(PROG);)

clean:
	rm -rf $(wildcard *.o) $(PROGS)
