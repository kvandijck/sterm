#include <errno.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <getopt.h>
#include <termios.h>
#include <sys/types.h>
#include <fcntl.h>
#include <poll.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>

#define NAME "sterm"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

/* generic error logging */
#define LOG_ERR	1
#define LOG_INFO 3
#define mylog(loglevel, fmt, ...) \
	({\
		fprintf(stderr, "%s: " fmt "\n", NAME, ##__VA_ARGS__);\
		fflush(stderr);\
		if (loglevel <= LOG_ERR)\
			exit(1);\
	})
#define ESTR(num)	strerror(num)

/* program options */
static const char help_msg[] =
	NAME ": raw serial terminal pogram\n"
	"usage:	" NAME " [OPTIONS ...] [-bBAUD[EON12DR]] [DEVICE] [ CMD [ ARGS ]]\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -b, --baud=BAUD	Use this baudrate (default 115200)\n"
	"			Optional suffixes:\n"
	"			E even parity\n"
	"			O odd parity\n"
	"			N no parity\n"
	"			1 1 stopbit\n"
	"			2 2 stopbit\n"
	"			D modem control (DTR/DSR)\n"
	"			R hardware flow control (RTS/CTS)\n"
	" -w, --eof=TIME	Exit TIME after an EOF condition of any side\n"
	" -x, --idle=TIME	Exit after TIME idle\n"
	" -c, --cr		Translate incoming NL to CR\n"
	" -W, --wait		Wait for DEVICE to appear\n"
	"\n"
	"Arguments\n"
	" DEVICE Device to use, defaults to /dev/ttyUSB0\n"
	" CMD [ARGS]	Command to run with stdin+stdout tied to the serial port\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },

	{ "baud", required_argument, NULL, 'b', },
	{ "eof", required_argument, NULL, 'w', },
	{ "idle", required_argument, NULL, 'x', },
	{ "cr", no_argument, NULL, 'c', },
	{ "wait", no_argument, NULL, 'W', },
	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "V?b:w:x:cW";

static const char *port;
static unsigned long baud = 115200;
	#define FL_RTSCTS	0x01
	#define FL_DTRDSR	0x02
	#define FL_EVEN		0x04
	#define FL_ODD		0x08
	#define FL_STOP		0x10
	#define FL_CR		0x20 /* translate CR */
	#define FL_BACKSPACE	0x40 /* translate linux backspace (0x7f) to ascii backspace (0x08) */
static int portflags = FL_BACKSPACE;
static int portwait;

static double eoftimeout = NAN;
static double idletimeout = NAN;

/* local state */
static int fd;
static int escaped;
static int isconsole;
static volatile int sigterm;
static struct termios cfgstdin;
static int inflags;

static char buf[2048]; /* buffer */

struct preset {
	unsigned long baud;
	int conf;
	int AT;
};

static const struct preset presets [] = {
	{       0,      B0, },
	{      50,     B50, },
	{      75,     B75, },
	{     110,    B110, },
	{     134,    B134, },
	{     150,    B150, },
	{     200,    B200, },
	{     300,    B300, },
	{     600,    B600, },
	{    1200,   B1200, 1, },
	{    1800,   B1800, 2, },
	{    2400,   B2400, 3, },
	{    4800,   B4800, 4, },
	{    9600,   B9600, 5, },
	{   19200,  B19200, 6, },
	{   38400,  B38400, 7, },
	{   57600,  B57600, 8, },
	{  115200, B115200, 9, },
	{  230400, B230400, },
	{  460800, B460800, },
	{  500000, B500000, },
	{  576000, B576000, },
	{  921600, B921600, },
	{ 1000000, B1000000, },
	{ 1152000, B1152000, },
	{ 1500000, B1500000, },
	{ 2000000, B2000000, },
	{ 2500000, B2500000, },
	{ 3000000, B3000000, },
	{ 3500000, B3500000, },
	{ 4000000, B4000000, },
	{  0, -1, },
};

static int baud2preset (unsigned long baud)
{
	const struct preset * lp;

	for (lp = presets; lp->conf >= 0; ++lp) {
		if (baud == lp->baud) {
			return lp->conf;
		}
	}
	return -1;
}

static struct lookup {
	const char * a;
	int i;
} const ctrls [] = {
	{ "dtr"  , TIOCM_DTR , },
	{ "dsr"  , TIOCM_DSR , },
	{ "rts"  , TIOCM_RTS , },
	{ "cts"  , TIOCM_CTS , },
	{ "cd"   , TIOCM_CAR , },
	{ "ri"   , TIOCM_RNG , },
	{ "line" , TIOCM_LE  , },
	{ "tx2"  , TIOCM_ST  , },
	{ "rx2"  , TIOCM_SR  , },
	{ 0, },
};

static int process_char (int c)
{
	int flags;
	int cmd = -1;
	const struct lookup *lp;
	char *str;
	static char sbuf[64];

	switch (c) {
	case 'd':
		cmd = TIOCM_DTR;
		break;
	case 'r':
		cmd = TIOCM_RTS;
		break;
	case 't':
		cmd = TIOCM_ST;
		break;
	case ' ':
		break;
	case '0':
		return 0;
	case 'c':
		sigterm = 1;
		return -1;
	case 'a':
		return 1; /* ctrl^a */
	default:
		return c;
	}
	if (ioctl(fd, TIOCMGET, &flags) < 0)
		mylog(LOG_ERR, "ioctl %s TIOCMGET: %s", port, ESTR(errno));
	if (cmd >= 0) {
		if (flags & cmd) {
			if (ioctl(fd, TIOCMBIC, &cmd) < 0)
				mylog(LOG_ERR, "ioctl %s TIOCMBIC %02x: %s", port, cmd, ESTR(errno));
		} else {
			if (ioctl(fd, TIOCMBIS, &cmd) < 0)
				mylog(LOG_ERR, "ioctl %s TIOCMBIS %02x: %s", port, cmd, ESTR(errno));
		}
		flags ^= cmd;
	}

	/* print state */
	str = sbuf;
	for (lp = ctrls; lp->a; ++lp) {
		if (lp >= ctrls)
			*str++ = ' ';
		str += sprintf(str, "%c%s", (flags & lp->i) ? '+' : '-', lp->a);
	}
	mylog(LOG_INFO, "state: %s", sbuf);
	return -1;
}

static int process_stdin (char *b, unsigned int sz)
{
	char *src, *dst;
	int c;

	for (src = b, dst = b; src < b+sz; ++src) {
		if (!escaped) {
			switch (*src) {
			case 1:
				escaped = 1;
				break;
			case 4:
				// ctrl^c, should not come here
				raise(SIGINT);
				//kill(getpid(), SIGINT);
				break;
			case '\n':
				*dst++ = (inflags & FL_CR) ? '\r' : *src;
				break;
			case 0x7f:
				*dst++ = (portflags & FL_BACKSPACE) ? '\b' : *src;
				break;
			default:
				*dst++ = *src;
				break;
			}
		} else {
			escaped = 0;
			c = process_char(*src);
			if (c >= 0)
				*dst++ = c;
		}
	}
	return dst - b;
}

static void sighandler(int sig)
{
	switch (sig) {
	case SIGUSR1:
		process_char(' ');
		break;
	case SIGINT:
		write(fd, ((const char []){ 3, }), 1);
		break;
	case SIGTERM:
	case SIGALRM:
		++sigterm;
		break;
	}
}

static void reset_term (void)
{
	tcsetattr(STDIN_FILENO, TCSANOW, &cfgstdin);
}

#define RETRY(x)	({ int _ret; do _ret = (x); while (_ret < 0 && errno == EINTR); _ret; })

static void schedule_timeout(double timeout, int exclusive)
{
	static int xdone; /* remember a final timeout */
	struct itimerval itv = {
		.it_value = {
			.tv_sec = timeout,
			.tv_usec = (long)(timeout * 1e6) % 1000000,
		},
	};

	if (isnan(timeout) || xdone)
		return;

	if (!itv.it_value.tv_sec && !itv.it_value.tv_usec)
		itv.it_value.tv_usec = 1;

	if (setitimer(ITIMER_REAL, &itv, NULL) < 0)
		mylog(LOG_ERR, "setitimer %.6lfs: %s", timeout, ESTR(errno));
	if (exclusive)
		/* make this a final timeout */
		xdone = 1;
}

int main (int argc, char * argv[])
{
	int opt, ret;
	char *endp;
	int flags;
	struct termios term;
	struct pollfd polls [2];

	const int *psig;
	struct sigaction sigact = {
		.sa_handler = sighandler,
	};

	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case 'b':
		if (*optarg == '?') {
			const struct preset * lp;

			mylog(LOG_INFO, "possible baud:");

			for (lp = presets; lp->conf >= 0; ++lp)
				mylog(LOG_INFO, "\t%lu", lp->baud);
			exit(1);
		}
		ret = strtoul(optarg, &endp, 10);
		if (endp == optarg)
			mylog(LOG_ERR, "bad format for baud '%s'", optarg);
		if (baud2preset(ret) < 0)
			mylog(LOG_ERR, "unknown baudrate %u", ret);
		baud = ret;
		for (; *endp; ++endp)
		switch (*endp) {
		case 'N': case 'n':
		case '1':
			break;
		case 'E': case 'e':
			portflags |= FL_EVEN;
			break;
		case 'O': case 'o':
			portflags |= FL_ODD;
			break;
		case '2':
			portflags |= FL_STOP;
			break;
		case 'R': case 'r':
			portflags |= FL_RTSCTS;
			break;
		case 'D': case 'd':
			portflags |= FL_DTRDSR;
			break;
		}
		break;
	case 'W':
		portwait = 1;
		break;
	case 'w':
		eoftimeout = strtod(optarg, NULL);
		break;
	case 'x':
		idletimeout = strtod(optarg, NULL);
		break;
	case 'c':
		inflags |= FL_CR;
		break;
	case '?':
		fputs(help_msg, stderr);
		exit(0);
	default:
		fprintf(stderr, "unknown option '%c'", opt);
		fputs(help_msg, stderr);
		exit(1);
		break;
	}

	for (; optind < argc; ++optind) {
		struct stat st;

		/* ignore seperator argument -- */
		if (!strcmp(argv[optind], "--"))
			continue;

		if (stat(argv[optind], &st))
			mylog(LOG_ERR, "stat %s failed: %s", argv[optind], ESTR(errno));

		if (S_ISCHR(st.st_mode))
			port = argv[optind];
		else if (S_ISREG(st.st_mode))
			/* leave optind < argc */
			break;
		else
			mylog(LOG_ERR, "I don't understand argument '%s'", argv[optind]);
	}
	if (!port) {
		const char *const ports[] = { "/dev/ttyS0", "/dev/ttyUSB0", "/dev/ttyACM0", NULL, };
		int j;
		struct stat st;

		for (; !port; poll(NULL, 0, 10)) {

			for (j = 0; ports[j]; ++j) {
				if (!stat(ports[j], &st)) {
					port = ports[j];
					mylog(LOG_INFO, "using port %s", port);
					break;
				}
			}
			if (!portwait)
				break;
		}
		if (!port)
			mylog(LOG_ERR, "please specify port");
	} else if (!portwait) {
		struct stat st;
		for (; stat(port, &st); poll(NULL, 0, 10));
	}

	/* fork if requested */
	if (optind < argc) {
		int sock[2];

		if (socketpair(AF_UNIX, SOCK_STREAM, 0, sock) < 0)
			mylog(LOG_ERR, "socketpair failed: %s", ESTR(errno));
		ret = fork();
		if (ret < 0)
			mylog(LOG_ERR, "fork failed: %s", ESTR(errno));
		if (!ret) {
			/* child process */
			dup2(sock[1], STDIN_FILENO);
			dup2(sock[1], STDOUT_FILENO);
			close(sock[0]);
			close(sock[1]);
			execvp(argv[optind], argv+optind);
			mylog(LOG_ERR, "execvp %s ...: %s", argv[optind], ESTR(errno));
		}
		/* parent */
		dup2(sock[0], STDIN_FILENO);
		dup2(sock[0], STDOUT_FILENO);
		close(sock[0]);
		close(sock[1]);
	}
	/* setup signals */
	sigfillset(&sigact.sa_mask);
	for (psig = (const int []){ SIGALRM, SIGTERM, SIGINT, SIGUSR1, 0, }; *psig; ++psig) {
		ret = sigaction(*psig, &sigact, NULL);
		if (ret)
			mylog(LOG_ERR, "sigaction %s failed: %s", strsignal(*psig), ESTR(errno));
	}

	fd = open(port, O_RDWR | O_NOCTTY | O_NONBLOCK);
	if (fd < 0)
		mylog(LOG_ERR, "open %s: %s", port, ESTR(errno));

	/* setup serial port config */
	if (tcgetattr(fd, &term) < 0)
		mylog(LOG_ERR, "tcgetattr %s: %s", port, ESTR(errno));
	if (cfsetispeed(&term, baud2preset(baud)) < 0)
		mylog(LOG_ERR, "cfsetspeed %s %lu: %s", port, baud, ESTR(errno));
	if (cfsetospeed(&term, baud2preset(baud)) < 0)
		mylog(LOG_ERR, "cfsetospeed %s %lu: %s", port, baud, ESTR(errno));

	term.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | IXON | INLCR | IGNCR | ICRNL | INPCK);
	term.c_oflag &= ~(OPOST);
	term.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);

	term.c_cflag &= ~(CSIZE | PARENB | CSTOPB | PARODD | CRTSCTS | CLOCAL);
	term.c_cflag |= CS8 | HUPCL | CREAD;
	term.c_cc[VMIN ] = 1;
	term.c_cc[VTIME] = 0;
	if (portflags & (FL_EVEN | FL_ODD)) {
		term.c_iflag |= INPCK;
		term.c_cflag |= PARENB;
	}
	if (portflags & FL_ODD)
		term.c_cflag |= PARODD;

	if (portflags & FL_STOP)
		term.c_cflag |= CSTOPB;
	if (portflags & FL_RTSCTS)
		term.c_cflag |= CRTSCTS;
	if (!(portflags & FL_DTRDSR))
		term.c_cflag |= CLOCAL;

	if (tcsetattr(fd, TCSAFLUSH, &term) < 0)
		mylog(LOG_ERR, "tcsetattr %s: %s", port, ESTR(errno));
#if 1
	/* make serial port nonblocking */
	flags = fcntl(fd, F_GETFL, 0);
	if (flags < 0)
		mylog(LOG_ERR, "fcntl %s F_GETFL: %s", port, ESTR(errno));
	flags |= O_NONBLOCK;
	if (fcntl(fd, F_SETFL, flags) < 0)
		mylog(LOG_ERR, "fcntl %s F_SETFL 0x%02x: %s", port, flags, ESTR(errno));
#endif
	/* setup interactive terminal */
	if (isatty(STDIN_FILENO)) {
		int in = STDIN_FILENO;

		if (tcgetattr(in, &term) < 0)
			mylog(LOG_ERR, "tcgetattr stdin: %s", ESTR(errno));
		cfgstdin = term;
		isconsole = 1;
		atexit(reset_term);

		term.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | ICRNL | IGNCR | IXON);
		//term.c_oflag &= ~(OPOST);
		term.c_iflag |= (portflags & FL_CR) ? INLCR : ICRNL;
		term.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
		term.c_cflag &= ~(CSIZE);
		term.c_cflag |= CS8;
		term.c_lflag |= ISIG;
		if (tcsetattr(in, TCSANOW, &term) < 0)
			mylog(LOG_ERR, "tcsetattr stdin: %s", ESTR(errno));
	}

	polls[0].fd = STDIN_FILENO;
	polls[0].events = POLLIN;
	polls[1].fd = fd;
	polls[1].events = POLLIN;

	while (!sigterm) {
		ret = poll(polls, 2, -1);
		if (ret < 0 && errno == EINTR)
			continue;
		if (ret < 0)
			mylog(LOG_ERR, "poll ...: %s", ESTR(errno));
		if (polls[1].revents) {
			ret = read(fd, buf, sizeof(buf));
			if (ret < 0 && errno == EINTR)
				continue;
			if (ret < 0 && errno == EAGAIN)
				continue; /* ?? */
			if (ret < 0)
				mylog(LOG_ERR, "read %s: %s", port, ESTR(errno));
			else if (!ret) {
				/* eof */
				/* our main reason to survive has stopped ... */
				break;
			} else if (ret > 0) {
				ret = RETRY(write(STDOUT_FILENO, buf, ret));
				if (ret < 0 && errno == EPIPE) {
					/* stop reading serial port */
					polls[1].events = 0;
					if (!polls[0].events)
						/* all ends are dead */
						break;
					schedule_timeout(eoftimeout, 1);
				} else if (ret < 0)
					mylog(LOG_ERR, "write stdout: %s", ESTR(errno));
				else
					schedule_timeout(idletimeout, 0);
			}
		}
		if (polls[0].revents) {
			ret = read(STDIN_FILENO, buf, sizeof(buf));
			if (ret < 0 && errno == EINTR)
				continue;
			if (ret < 0)
				mylog(LOG_ERR, "read %s: %s", port, ESTR(errno));
			else if (!ret) {
				/* eof */
				polls[0].events = 0;
				if (!polls[1].events)
					/* all ends are dead */
					break;
				/* schedule timeout in 1 second */
				schedule_timeout(eoftimeout, 1);
			} else if (ret > 0) {
				if (isconsole)
					ret = process_stdin(buf, ret);
				if (!ret)
					/* all chars consumed */
					continue;
				ret = RETRY(write(fd, buf, ret));
				if (ret < 0)
					mylog(LOG_ERR, "write %s: %s", port, ESTR(errno));
				schedule_timeout(idletimeout, 0);
			}
		}
	}
	close(fd);
	return 0;
}

