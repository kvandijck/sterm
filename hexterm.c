#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <getopt.h>
#include <poll.h>
#include <sys/stat.h>

/* generic error logging */
#define elog(exitcode, errnum, fmt, ...) \
	({\
		fprintf(stderr, "%s: " fmt "\n", NAME, ##__VA_ARGS__);\
		if (errnum)\
			fprintf(stderr, "\t: %s\n", strerror(errnum));\
		if (exitcode)\
			exit(exitcode);\
		fflush(stderr);\
	})

#define NAME "hexterm"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

/* convert baud setting */
struct baudsetting {
	unsigned long baud;
	int macro;
	int AT;
};
static const struct baudsetting baudsettings[] = {
	//{       0,      B0, },
	{      50,     B50, },
	{      75,     B75, },
	{     110,    B110, },
	{     134,    B134, },
	{     150,    B150, },
	{     200,    B200, },
	{     300,    B300, },
	{     600,    B600, },
	{    1200,   B1200, 1, },
	{    1800,   B1800, 2, },
	{    2400,   B2400, 3, },
	{    4800,   B4800, 4, },
	{    9600,   B9600, 5, },
	{   19200,  B19200, 6, },
	{   38400,  B38400, 7, },
	{   57600,  B57600, 8, },
	{  115200, B115200, 9, },
	{  230400, B230400, },
	{},
};

static int baud2setting(unsigned long baud)
{
	const struct baudsetting *bset;

	for (bset = baudsettings; bset->baud; ++bset) {
		if (baud == bset->baud) {
			return bset->macro;
		}
	}
	return -1;
}

/* program options */
static const char help_msg[] =
	NAME ": serial terminal pogram with hexadecimal input/output\n"
	"usage:	" NAME " [OPTIONS ...] [DEVICE]\n"
	"\n"
	"Options\n"
	" -V, --version	Show version\n"
	" -v, --verbose	Verbose output\n"
	" -b, --baud=RATE[P[S] Operate on RATE (default 115200)\n"
	"		optionally add parity P: Even, Odd, None\n"
	"		and Stopbits S: 1 or 2\n"
	" -r, --rtscts	Use RTS/CTS flow control\n"
	" -m, --modem	Use DTR/DSR lines\n"
	" -e, --even	Use EVEN parity (default NONE)\n"
	" -o, --odd	Use ODD parity (default NONE)\n"
	" -s, --stop	Use 2 stopbits (default 1)\n"
	" -w, --wait=MSEC	Wait up to MSEC for filling the serial buffer (default 10)\n"

	"\n"
	"Arguments\n"
	" DEVICE	Device to use, defaults to /dev/ttyUSB0\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "baud", required_argument, NULL, 'b', },
	{ "rtscts", no_argument, NULL, 'r', },
	{ "modem", no_argument, NULL, 'm', },
	{ "even", no_argument, NULL, 'e', },
	{ "odd", no_argument, NULL, 'o', },
	{ "stop", no_argument, NULL, 's', },

	{ "wait", required_argument, NULL, 'w', },
	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vv?b:rmeosw:";

/* devices to probe */
static const char *const probedevices[] = { "/dev/ttyS0", "/dev/ttyUSB0" ,"/dev/ttyACM0", NULL, };

/* program arguments */
int verbose;
static const char *device;
static int portflags;
	#define FL_RTSCTS	0x01
	#define FL_DTRDSR	0x02
	#define FL_EVEN		0x04
	#define FL_ODD		0x08
	#define FL_STOP		0x10
static int baud = B115200;
static int bufwait = 10;

/* hex/str conversions */
static inline int c2i(int chr)
{
	if ((chr >= '0') && (chr <= '9'))
		return chr - '0';
	else if ((chr >= 'a') && (chr <= 'z'))
		return chr - 'a' + 10;
	else if ((chr >= 'A') && (chr <= 'Z'))
		return chr - 'Z' + 10;
	return -1;
}

static int consumehex(char *str, int *pos)
{
	static const char hexdigits[] = "0123456789abcdefABCDEF";

	char *mystr;
	uint8_t dat;

	mystr = strpbrk(str + *pos, hexdigits);
	if (!mystr)
		return -1;
	dat = c2i(*mystr++);
	if (strchr(hexdigits, *mystr)) {
		dat <<= 4;
		dat += c2i(*mystr++);
	}
	*pos = mystr - str;
	return dat;
}

static int producehex(char *buf, size_t bufsize, const void *bin, size_t binsize)
{
	const uint8_t *dat = bin;
	char *str;
	size_t j;

	for (str = buf, j = 0; j < binsize; ++j) {
		if (j && !(j % 2))
			*str++ = ' ';
		str += sprintf(str, "%02x", dat[j]);
	}
	*str = 0;
	return str - buf;
}

/* local data */
static uint8_t dat[128];
static int tdatlen;
static uint8_t tdat[128];
static char line[1024];

int main(int argc, char *argv[])
{
	int opt, ret, sock, j, pos;
	struct stat st;
	struct pollfd polls[2] = {
		[0] = { .fd = STDIN_FILENO, .events = POLLIN, },
		[1] = { .events = POLLIN, },
	};
	struct termios tcattr;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case 'v':
		++verbose;
		break;

	case 'b':
		baud = baud2setting(strtoul(optarg, NULL, 0));
		if (baud < 0)
			elog(1, 0, "'%s' is not a known baudrate", optarg);
		break;
	case 'r':
		portflags |= FL_RTSCTS;
		break;
	case 'm':
		portflags |= FL_DTRDSR;
		break;
	case 'e':
		portflags |= FL_EVEN;
		break;
	case 'o':
		portflags |= FL_ODD;
		break;
	case 's':
		portflags |= FL_STOP;
		break;
	case 'w':
		bufwait = strtoul(optarg, NULL, 0);
		break;

	case '?':
		fputs(help_msg, stderr);
		exit(0);
	default:
		fprintf(stderr, "unknown option '%c'", opt);
		fputs(help_msg, stderr);
		exit(1);
		break;
	}

	if (argv[optind])
		device = argv[optind];
	else for (j = 0; probedevices[j]; ++j) {
		if (!stat(probedevices[j], &st)) {
			if (device)
				elog(1, 0, "specify device, one of %s, %s, ...",
						device, probedevices[j]);
			device = probedevices[j];
		}
	}
	if (!device)
		elog(1, 0, "no device");

	ret = sock = open(device, O_RDWR | O_NOCTTY | O_NONBLOCK);
	if (ret < 0)
		elog(1, errno, "open %s", device);

	/* setup terminal */
	if (tcgetattr(sock, &tcattr) < 0)
		elog(1, errno, "tcgetattr %s", device);

	cfsetispeed(&tcattr, baud);
	cfsetospeed(&tcattr, baud);

	tcattr.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | IXON | INLCR | IGNCR | ICRNL | INPCK);
	tcattr.c_oflag &= ~(OPOST);
	tcattr.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);

	tcattr.c_cflag &= ~(CSIZE | PARENB | CSTOPB | PARODD | CRTSCTS | CLOCAL);
	tcattr.c_cflag |= CS8 | HUPCL | CREAD;
	tcattr.c_cc[VMIN ] = 1;
	tcattr.c_cc[VTIME] = 0;

	if (portflags & FL_EVEN) {
		tcattr.c_iflag |= INPCK;
		tcattr.c_cflag |= PARENB;
	}
	if (portflags & FL_ODD) {
		tcattr.c_iflag |= INPCK;
		tcattr.c_cflag |= PARENB | PARODD;
	}
	if (portflags & FL_STOP)
		tcattr.c_cflag |= CSTOPB;
	if (portflags & FL_RTSCTS)
		tcattr.c_cflag |= CRTSCTS;
	if (!(portflags & FL_DTRDSR))
		tcattr.c_cflag |= CLOCAL;

	if (tcsetattr(sock, TCSAFLUSH, &tcattr) < 0)
		elog(1, errno, "tcsetattr(%s)", device);

	if (fcntl(sock, F_SETFL, fcntl(sock, F_GETFL) & ~O_NONBLOCK) < 0)
		elog(1, errno, "set %s blocking", device);

	polls[1].fd = sock;

	while (1) {
		ret = poll(polls, sizeof(polls)/sizeof(polls[1]), -1);
		if ((ret < 0) && (errno == EINTR))
			continue;
		if (ret < 0)
			elog(1, errno, "poll");
		if (polls[0].revents) {
			for (pos = 0; pos < sizeof(line)-1; ) {
				ret = read(STDIN_FILENO, line+pos, sizeof(line)-1-pos);
				if (ret < 0)
					elog(1, errno, "read stdin\n");
				pos += ret;
				if (!ret || poll(polls, 1, bufwait) < 1)
					/* nothing to read anymore */
					break;
			}
			if (!pos)
				break;
			line[pos] = 0;
			for (j = 0, pos = 0; j < sizeof(tdat); ++j) {
				ret = consumehex(line, &pos);
				if (ret < 0)
					break;
				tdat[j] = ret;
			}
			if (j)
				/* empty command repeats previous */
				tdatlen = j;
			if (tdatlen) {
				ret = write(sock, tdat, tdatlen);
				if (ret < 0)
					elog(1, errno, "write %s", device);
				producehex(line, sizeof(line), tdat, tdatlen);
				printf("< %s\n", line);
			}
		}
		if (polls[1].revents) {
			/* read buf until nothing comes */
			pos = 0;
			do {
				ret = read(sock, dat+pos, sizeof(dat)-pos);
				if (ret < 0)
					elog(1, errno, "read %s", device);
				else if (!ret && !pos)
					elog(1, 0, "eof %s", device);
				pos += ret;
			} while (pos < sizeof(dat) && poll(polls+1, 1, bufwait) > 0);

			producehex(line, sizeof(line), dat, pos);
			printf("> %s\n", line);
		}
		fflush(stdout);
	}

	return 0;
}
